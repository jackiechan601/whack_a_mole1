#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Moles
{
public:
	// Constructor
	Moles(std::vector<sf::Texture> & moleTextures, sf::Vector2u screenSize);

	// Variables (data members) used by this class:
	sf::Sprite mole;
	sf::Sprite mole2;
	sf::Sprite mole3;
	sf::Sprite mole4;
	sf::Sprite mole5;
	sf::Sprite mole6;
	sf::Sprite mole7;
	sf::Sprite mole8;
	sf::Sprite mole9;
	int pointsValue;	int timeValue;	int x = 0;	int y = 0;	bool activeHole = false;
	int random = 0;
};

