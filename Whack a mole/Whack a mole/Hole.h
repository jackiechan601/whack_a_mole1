#pragma once
#include <SFML/Graphics.hpp>
#include <vector>
class Hole
{
public:
	// Constructor
	Hole(std::vector<sf::Texture> & holeTextures, sf::Vector2u screenSize);

	// Variables (data members) used by this class:
	sf::Sprite hole;
	sf::Sprite hole2;
	sf::Sprite hole3;
	sf::Sprite hole4;
	sf::Sprite hole5;
	sf::Sprite hole6;
	sf::Sprite hole7;
	sf::Sprite hole8;
	sf::Sprite hole9;
};

