#include "Hole.h"
#include <cstdlib>
Hole::Hole(std::vector<sf::Texture>& holeTextures, sf::Vector2u screenSize)
{
	hole.setTexture(holeTextures[0]);
	hole2.setTexture(holeTextures[1]);
	hole3.setTexture(holeTextures[2]);
	hole4.setTexture(holeTextures[3]);
	hole5.setTexture(holeTextures[4]);
	hole6.setTexture(holeTextures[5]);
	hole7.setTexture(holeTextures[6]);
	hole8.setTexture(holeTextures[7]);
	hole9.setTexture(holeTextures[8]);
	// Choose random x and y positions based on screen size, reduced by the size of the texture to avoid spawning partially off screen.
	//int positionX = 100;
	//int positionY = 100;
	//sprite.setPosition(positionX, positionY);
	hole.setPosition(100, 100);
	hole2.setPosition(350, 100);
	hole3.setPosition(600, 100);
	hole4.setPosition(100, 300);
	hole5.setPosition(350, 300);
	hole6.setPosition(600, 300);
	hole7.setPosition(100, 500);
	hole8.setPosition(350, 500);
	hole9.setPosition(600, 500);
}