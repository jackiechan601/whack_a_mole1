﻿#include <SFML/Graphics.hpp>
#include <SFML/Audio.hpp>
#include <string>
#include <vector>
#include "Player.h"
#include "Moles.h"
#include "Hole.h"
#include <cstdlib>

int main()
{
	{
	// Declare our SFML window, called gameWindow
	sf::RenderWindow gameWindow;

	// Set up the SFML window, passing the dimmensions and the window name
	gameWindow.create(sf::VideoMode(800, 600), "Whack a mole", sf::Style::Titlebar | sf::Style::Close);
	// Game Setup

	// Player
	// Declare a texture variable called playerTexture
	sf::Texture playerTexture;
	// Load up our texture from a file path
	playerTexture.loadFromFile("Assets/Graphics/player.png");
	// Declare a player object
	Player playerObject(playerTexture, gameWindow.getSize());

	// Game Music
	// Declare a music variable called gameMusic
	sf::Music gameMusic;
	// Load up our audio from a file path
	gameMusic.openFromFile("Assets/Audio/music.ogg");
	// Start our music playing
	gameMusic.play();

	// Game Font
	// Declare a font variable called gameFont
	sf::Font gameFont;
	// Load up the font from a file path
	gameFont.loadFromFile("Assets/Fonts/mainFont.ttf");
	// Title Text
	// Declare a text variable called titleText to hold our game title display
	sf::Text titleText;
	// Set the font our text should use
	titleText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	titleText.setString("Whack a mole");
	// Set the size of our text, in pixels
	titleText.setCharacterSize(24);
	// Set the colour of our text
	titleText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	titleText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	titleText.setPosition(gameWindow.getSize().x / 2 - titleText.getLocalBounds().width / 2, 30);
	// Author Text
	sf::Text authorText;
	authorText.setFont(gameFont);
	authorText.setString("by Jack Greenhill-Lally");
	authorText.setCharacterSize(16);
	authorText.setFillColor(sf::Color::Magenta);
	authorText.setStyle(sf::Text::Italic);
	authorText.setPosition(gameWindow.getSize().x / 2 - authorText.getLocalBounds().width / 2, 60);

	// Score
	// Declare an integer variable to hold our numerical score value
	int score = 0;
	// Setup score text object
	sf::Text scoreText;
	scoreText.setFont(gameFont);
	scoreText.setString("Score: 0");
	scoreText.setCharacterSize(16);
	scoreText.setFillColor(sf::Color::White);
	scoreText.setPosition(30, 30);

	// Timer
	sf::Text timerText;
	timerText.setFont(gameFont);
	timerText.setString("Time Remaining: 0");
	timerText.setCharacterSize(16);
	timerText.setFillColor(sf::Color::White);
	timerText.setPosition(gameWindow.getSize().x - timerText.getLocalBounds().width - 30, 30);
	// Create a time value to store the total time limit for our game
	sf::Time timeLimit = sf::seconds(60.0f);
	// Create a timer to store the time remaining for our game
	sf::Time timeRemaining = timeLimit;
	sf::Time timeAdded = sf::seconds(5.0f);

	// Game Clock
	// Create a clock to track time passed each frame in the game
	sf::Clock gameClock;
	// Seed the random number generator
	srand(time(NULL));

	// moles
	// Load all textures that will be used by our moles
	std::vector<sf::Texture> moleTextures;
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures.push_back(sf::Texture());
	moleTextures[0].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[1].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[2].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[3].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[4].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[5].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[6].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[7].loadFromFile("Assets/Graphics/Mole.png");
	moleTextures[8].loadFromFile("Assets/Graphics/Mole.png");
	// Create the vector to hold our moles
	std::vector<Moles> Mole;
	// Load up a few starting moles
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
	Mole.push_back(Moles(moleTextures, gameWindow.getSize()));

	// Holes
	// Load all textures that will be used by our holes
	std::vector<sf::Texture> holeTextures;
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures.push_back(sf::Texture());
	holeTextures[0].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[1].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[2].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[3].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[4].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[5].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[6].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[7].loadFromFile("Assets/Graphics/Hole.png");
	holeTextures[8].loadFromFile("Assets/Graphics/Hole.png");
	// Create the vector to hold our moles
	std::vector<Hole> Holes;
	// Load up a few starting moles
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));
	Holes.push_back(Hole(holeTextures, gameWindow.getSize()));

	// Create a time value to store the total time between each mole spawn
	sf::Time moleSpawnDuration = sf::seconds(2.0f);

	// Create a timer to store the time remaining for our game
	sf::Time moleSpawnRemaining = moleSpawnDuration;

	// Load the pickup sound effect file into a soundBuffer
	sf::SoundBuffer pickupSoundBuffer;
	pickupSoundBuffer.loadFromFile("Assets/Audio/pickup.wav");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound pickupSound;
	pickupSound.setBuffer(pickupSoundBuffer);

	// Load the victory sound effect file into a soundBuffer
	sf::SoundBuffer victorySoundBuffer;
	victorySoundBuffer.loadFromFile("Assets/Audio/victory.ogg");
	// Setup a Sound object to play the sound later, and associate it with the SoundBuffer
	sf::Sound victorySound;
	victorySound.setBuffer(victorySoundBuffer);

	// Game over variable to track if the game is done
	bool gameOver = false;

	// Game Over Text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text gameOverText;
	// Set the font our text should use
	gameOverText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	gameOverText.setString("GAME OVER");
	// Set the size of our text, in pixels
	gameOverText.setCharacterSize(72);
	// Set the colour of our text
	gameOverText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	gameOverText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	gameOverText.setPosition(gameWindow.getSize().x / 2 - gameOverText.getLocalBounds().width / 2, 150);

	// Restart Text
	// Declare a text variable called gameOverText to hold our game over display
	sf::Text restartText;
	// Set the font our text should use
	restartText.setFont(gameFont);
	// Set the string of text that will be displayed by this text object
	restartText.setString("Press R To Restart");
	// Set the size of our text, in pixels
	restartText.setCharacterSize(72);
	// Set the colour of our text
	restartText.setFillColor(sf::Color::Cyan);
	// Set the text style for the text
	restartText.setStyle(sf::Text::Bold | sf::Text::Italic);
	// Position our text in the top center of the screen
	restartText.setPosition(gameWindow.getSize().x / 2 - restartText.getLocalBounds().width / 2, 200);
	
	// Game Loop
	// Repeat as long as the window is open
	while (gameWindow.isOpen())
	{
		// Input Section
		// Check if we should reset the game
		if (gameOver && sf::Keyboard::isKeyPressed(sf::Keyboard::R))
		{
			// Reset the game
			score = 0;
			timeRemaining = timeLimit;
			Mole.clear();
			Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
			gameMusic.play();
			gameOver = false;
			playerObject.Reset(gameWindow.getSize());
		}
		// Declare a variable to hold an Event, called gameEvent
		sf::Event gameEvent;
		// Loop through all events and poll them, putting each one into our gameEvent variable
		while (gameWindow.pollEvent(gameEvent))
		{
			// This section will be repeated for each event waiting to be processed
				// Did the player try to close the window?
			if (gameEvent.type == sf::Event::Closed)
			{
				// If so, call the close function on the window.
				gameWindow.close();
			}
		}
		// End event polling loop

		// Player keybind input
		playerObject.Input();
		// Update Section
		// Check for collisions
		for (int i = 0; i < Mole.size(); ++i)
		{
			sf::FloatRect moleBounds = Mole[i].mole.getGlobalBounds();
			sf::FloatRect mole2Bounds = Mole[i].mole2.getGlobalBounds();
			sf::FloatRect mole3Bounds = Mole[i].mole3.getGlobalBounds();
			sf::FloatRect mole4Bounds = Mole[i].mole4.getGlobalBounds();
			sf::FloatRect mole5Bounds = Mole[i].mole5.getGlobalBounds();
			sf::FloatRect mole6Bounds = Mole[i].mole6.getGlobalBounds();
			sf::FloatRect mole7Bounds = Mole[i].mole7.getGlobalBounds();
			sf::FloatRect mole8Bounds = Mole[i].mole8.getGlobalBounds();
			sf::FloatRect mole9Bounds = Mole[i].mole9.getGlobalBounds();
			sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
			if (moleBounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole2Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole3Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole4Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole5Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole6Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole7Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole8Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}

			if (mole9Bounds.intersects(playerBounds))
			{
				// Our player touched the mole
				// Add the moles value to the player score
				score += Mole[i].pointsValue;
				timeRemaining += timeAdded;
				// Remove the touched mole
				Mole.erase(Mole.begin() + i);
				// Play the pickup sound
				pickupSound.play();
			}
		}

		// Check if time has run out
		if (timeRemaining.asSeconds() <= 0)
		{
			// Don't let the time go lower than 0
			timeRemaining = sf::seconds(0);
			// Perform these actions only once when the game first ends:
			if (gameOver == false)
			{
				// Set our gameOver to true now so we don't perform these actions again
				gameOver = true;
				// Stop the main music from playing
				gameMusic.stop();
				// Play our victory sound
				victorySound.play();
			}
		}

		// Get the time passed since the last frame and restart our game clock
		sf::Time frameTime = gameClock.restart();
		// Update our time remaining based on how much time passed last frame
		timeRemaining = timeRemaining - frameTime;
		// Update our time display based on our time remaining
		timerText.setString("Time Remaining: " + std::to_string((int)timeRemaining.asSeconds()));
		// Update our score display text based on our current numerical score
		scoreText.setString("Score: " + std::to_string(score));
		// Update our mole spawn time remaining based on how much time passed last frame
		moleSpawnRemaining = moleSpawnRemaining - frameTime;
		// Check if time remaining to next spawn has reached 0
		if (moleSpawnRemaining <= sf::seconds(0.0f))
		{
			// Time to spawn a new mole!
			Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
			// Reset time remaining to full duration
			moleSpawnRemaining = moleSpawnDuration;
		}

		// Only perform this update logic if the game is still running:
		if (!gameOver)
		{
			// Update our mole spawn time remaining based on how much time passed last frame
			moleSpawnRemaining = moleSpawnRemaining - frameTime;
			// Check if time remaining to next spawn has reached 0
			if (moleSpawnRemaining <= sf::seconds(0.0f))
			{
				// Time to spawn a new mole!
				Mole.push_back(Moles(moleTextures, gameWindow.getSize()));
				// Reset time remaining to full duration
				moleSpawnRemaining = moleSpawnDuration;
			}
			// Move the player
			playerObject.Update(frameTime);
			// Check for collisions
			for (int i = Mole.size() - 1; i >= 0; --i)
			{
				sf::FloatRect moleBounds = Mole[i].mole.getGlobalBounds();
				sf::FloatRect playerBounds = playerObject.sprite.getGlobalBounds();
				if (moleBounds.intersects(playerBounds))
				{
					// Our player touched the mole!
					// Add the mole's value to the score
					score += Mole[i].pointsValue;
					// Remove the mole from the vector
					Mole.erase(Mole.begin() + i);
					// Play the pickup sound
					pickupSound.play();
				}
			}
		}
		// Move the player
		playerObject.Update(frameTime);

		// Draw Section
		// Clear the window to a single colour
		gameWindow.clear(sf::Color::Black);
		// Draw everything to the window
		gameWindow.draw(titleText);
		gameWindow.draw(authorText);
		gameWindow.draw(scoreText);
		gameWindow.draw(timerText);
		// Only draw these moles if the game has NOT ended:
		if (!gameOver)
		{
			for (int i = 0; i < Holes.size(); ++i)
			{
				gameWindow.draw(Holes[i].hole);
				gameWindow.draw(Holes[i].hole2);
				gameWindow.draw(Holes[i].hole3);
				gameWindow.draw(Holes[i].hole4);
				gameWindow.draw(Holes[i].hole5);
				gameWindow.draw(Holes[i].hole6);
				gameWindow.draw(Holes[i].hole7);
				gameWindow.draw(Holes[i].hole8);
				gameWindow.draw(Holes[i].hole9);
			}
			gameWindow.draw(playerObject.sprite);
			// Draw all our moles
			for (int i = 0; i < Mole.size(); ++i)
			{
				gameWindow.draw(Mole[i].mole);
				gameWindow.draw(Mole[i].mole2);
				gameWindow.draw(Mole[i].mole3);
				gameWindow.draw(Mole[i].mole4);
				gameWindow.draw(Mole[i].mole5);
				gameWindow.draw(Mole[i].mole6);
				gameWindow.draw(Mole[i].mole7);
				gameWindow.draw(Mole[i].mole8);
				gameWindow.draw(Mole[i].mole9);
			}
			
		}
		// Only draw these moles if the game HAS ended:
		if (gameOver)
		{
			gameWindow.draw(gameOverText);
			gameWindow.draw(restartText);
		}
		// Display the window contents on the screen
		gameWindow.display();
	}
	// End of Game Loop
	}
}